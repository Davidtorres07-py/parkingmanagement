package com.test.parking.staff.service;

import com.test.parking.staff.model.entity.ResidentPaymentEntity;
import com.test.parking.staff.model.entity.StayRegistrationEntity;
import com.test.parking.staff.repository.ResidentPaymentRepository;
import com.test.parking.staff.repository.StayRegistrationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@Service
public class ResidentPaymentService {

    private final ResidentPaymentRepository residentPaymentRepository;
    private final StayRegistrationService stayRegistrationService;
    private final StayRegistrationRepository stayRegistrationRepository;

    public ResidentPaymentService(ResidentPaymentRepository residentPaymentRepository, StayRegistrationService stayRegistrationService, StayRegistrationRepository stayRegistrationRepository) {
        this.residentPaymentRepository = residentPaymentRepository;
        this.stayRegistrationService = stayRegistrationService;
        this.stayRegistrationRepository = stayRegistrationRepository;
    }

    public void generateResidentPaymentReport(String fileName, List<StayRegistrationEntity> registrations){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write("Placa\tTiempo Estacionado (minutos)\tMonto a Pagar (MXN)\n");

            List<ResidentPaymentEntity> registList = residentPaymentRepository.findAll();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            for (ResidentPaymentEntity registration : registList) {

                writer.write(String.format("%-10s %-25d %-10.2f\n", registration.getMatricula(), registration.getTiempo_estacionado(),
                        registration.getTotal()));
            }
            System.out.println("Informe generado exitosamente. " + fileName);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Error al generar informe");
        }
    }


    private long parkingTimeCalculator(LocalDateTime entrada, LocalDateTime salida){
        return java.time.Duration.between(entrada,salida).toMinutes();
    }

    public List<StayRegistrationEntity> getAllRegistrations() {
        try {
            List<StayRegistrationEntity> registrations = stayRegistrationRepository.findAll();
            if (registrations.isEmpty()) {
                log.info("No se encontraron registros de estancia");
            }
            return registrations;
        } catch (Exception e) {
            log.error("Error al obtener todas las registraciones", e);
            throw new RuntimeException("Error al obtener todas las registraciones", e);
        }
    }

}
