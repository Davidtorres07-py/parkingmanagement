package com.test.parking.staff.service;

import com.test.parking.staff.model.entity.OfficialVehiclesEntity;
import com.test.parking.staff.model.entity.ResidentPaymentEntity;
import com.test.parking.staff.model.entity.ResidentVehiclesEntity;
import com.test.parking.staff.model.entity.VehicleEntity;
import com.test.parking.staff.repository.ResidentPaymentRepository;
import com.test.parking.staff.repository.ResidentVehicleRepository;
import com.test.parking.staff.repository.VehicleRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j

public class ResidentVehicleService {

    private final ResidentVehicleRepository residentVehicleRepository;
    private final VehicleRepository vehicleRepository;
    private final ResidentPaymentRepository residentPaymentRepository;


    public ResidentVehicleService(ResidentVehicleRepository residentVehicleRepository, VehicleRepository vehicleRepository, ResidentPaymentRepository residentPaymentRepository) {
        this.residentVehicleRepository = residentVehicleRepository;
        this.vehicleRepository = vehicleRepository;
        this.residentPaymentRepository = residentPaymentRepository;
    }


    @Transactional
    public ResponseEntity<ResidentVehiclesEntity> createResidentVehicle(String placa){
        try {
            VehicleEntity vehicleEntity = new VehicleEntity();
            vehicleEntity.setPlaca(placa);
            vehicleEntity.setId_tipo(1);
            vehicleRepository.save(vehicleEntity);

            ResidentVehiclesEntity residentVehiclesEntity = new ResidentVehiclesEntity();
            residentVehiclesEntity.setMatricula(placa);
            residentVehiclesEntity.setId_vehiculo(vehicleEntity.getId());
            ResidentVehiclesEntity savedResidentVehicle = residentVehicleRepository.save(residentVehiclesEntity);

            ResidentPaymentEntity residentPaymentEntity = new ResidentPaymentEntity();
            residentPaymentEntity.setId_vehiculo(vehicleEntity.getId());
            residentPaymentEntity.setMatricula(placa);
            residentPaymentEntity.setTiempo_estacionado(0L);
            residentPaymentEntity.setTotal(0.00);
            residentPaymentRepository.save(residentPaymentEntity);

            return ResponseEntity.status(HttpStatus.CREATED).body(savedResidentVehicle);
        } catch (DataAccessException ex){
            log.error("An error occurred while saving the new resident vehicle " + ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Transactional
    public Boolean deleteResidentVehicle(Long id){
        try {
            ResidentVehiclesEntity residentVehiclesEntity = residentVehicleRepository.findById(id).orElse(null);
            Long idVehicle = vehicleRepository.findIdByPlaca(residentVehiclesEntity.getMatricula());

            Optional<ResidentPaymentEntity> residentPaymentEntity = residentPaymentRepository.findByMatricula(residentVehiclesEntity.getMatricula());
            Long idResidentPayment = residentPaymentEntity.get().getId();

            VehicleEntity vehicleEntity = vehicleRepository.findById(idVehicle).orElse(null);

            if (residentVehiclesEntity != null){
                residentVehicleRepository.delete(residentVehiclesEntity);
                residentPaymentRepository.deleteById(idResidentPayment);
                vehicleRepository.delete(vehicleEntity);


                return true;
            }
            return false;
        } catch (Exception e){
            log.error("An error occurred while deleting resident vehicle " + e);
            return false;
        }
    }

    public List<String> getAllResidentVehicles(){
        List<ResidentVehiclesEntity> residentVehiclesList = residentVehicleRepository.findAll();
        List<String> residentVehiclePlates = new ArrayList<>();

        if (residentVehiclesList.isEmpty()){
            log.info("No se encontraron vehículos de residentes");
        } else {
            log.info("Se encontraron {} vehículos de residentes", residentVehiclesList.size());
            for (ResidentVehiclesEntity residentVehicle : residentVehiclesList) {
                String placa = residentVehicle.getMatricula();
                residentVehiclePlates.add(placa);
            }
        }
        return residentVehiclePlates;
    }


}
