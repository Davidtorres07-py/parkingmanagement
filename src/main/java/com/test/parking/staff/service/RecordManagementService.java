package com.test.parking.staff.service;

import com.test.parking.staff.model.entity.ResidentPaymentEntity;
import com.test.parking.staff.repository.OfficialRecordRepository;
import com.test.parking.staff.repository.ResidentPaymentRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class RecordManagementService {

    private final ResidentPaymentRepository residentPaymentRepository;
    private final OfficialRecordRepository officialRecordRepository;


    public RecordManagementService(ResidentPaymentService residentPaymentService, ResidentPaymentRepository residentPaymentRepository, OfficialRecordRepository officialRecordRepository) {
        this.residentPaymentRepository = residentPaymentRepository;
        this.officialRecordRepository = officialRecordRepository;
    }

    public void startNewMonth() {
        clearOfficialRecords();
        resetResidentVehicleTimes();
    }

    @Transactional
    public void clearOfficialRecords() {
        try {
            officialRecordRepository.deleteAll();
            log.info("Registros de oficiales eliminados correctamente");
        } catch (Exception e){
            log.error("Error al eliminar registros de oficiales. "+ e);
            throw new RuntimeException();
        }
    }

    @Transactional
    public void resetResidentVehicleTimes() {
        try {
            List<ResidentPaymentEntity> residentPayments = residentPaymentRepository.findAll();
            for (ResidentPaymentEntity residentPayment : residentPayments) {
                residentPayment.setTiempo_estacionado(0L);
                residentPayment.setTotal(0.00);
                residentPaymentRepository.save(residentPayment);
            }
            log.info("Tiempo estacionado por vehículos de residentes puesto a cero correctamente.");
        } catch (Exception e) {
            log.error("Error al poner a cero el tiempo estacionado por vehículos de residentes: " + e.getMessage());
            throw new RuntimeException("Error al poner a cero el tiempo estacionado por vehículos de residentes.", e);
        }
    }

}
