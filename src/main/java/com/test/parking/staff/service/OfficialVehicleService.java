package com.test.parking.staff.service;
import com.test.parking.staff.model.entity.OfficialVehiclesEntity;
import com.test.parking.staff.model.entity.VehicleEntity;
import com.test.parking.staff.model.request.OfficialVehicleRequest;
import com.test.parking.staff.repository.OfficialVehiclesRepository;
import com.test.parking.staff.repository.VehicleRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service

public class OfficialVehicleService {

    private final OfficialVehiclesRepository officialVehiclesRepository;
    private final VehicleRepository vehicleRepository;

    public OfficialVehicleService(OfficialVehiclesRepository officialVehiclesRepository, VehicleRepository vehicleRepository) {
        this.officialVehiclesRepository = officialVehiclesRepository;
        this.vehicleRepository = vehicleRepository;
    }

    public Boolean isRequestValid(OfficialVehicleRequest request){
        return request.getPlaca() != null;
    }

    @Transactional
    public ResponseEntity<OfficialVehiclesEntity> createOfficialVehicle(OfficialVehicleRequest request){

        try {
            VehicleEntity vehicleEntity = new VehicleEntity();
            vehicleEntity.setPlaca(request.getPlaca());
            vehicleEntity.setId_tipo(2);
            vehicleRepository.save(vehicleEntity);

            OfficialVehiclesEntity officialVehiclesEntity = new OfficialVehiclesEntity();
            officialVehiclesEntity.setMatricula(request.getPlaca());
            officialVehiclesEntity.setId_vehiculo(vehicleEntity.getId());
            OfficialVehiclesEntity savedOfficialVehicle = officialVehiclesRepository.save(officialVehiclesEntity);

            return ResponseEntity.status(HttpStatus.CREATED).body(savedOfficialVehicle);
        } catch (DataAccessException ex){
            log.error("An error occurred while saving the new official vehicle " + ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Transactional
    public Boolean deleteOfficialVehicle(Long id){
        try {
            OfficialVehiclesEntity officialVehiclesEntity = officialVehiclesRepository.findById(id).orElse(null);
            if (officialVehiclesEntity != null){
                officialVehiclesRepository.delete(officialVehiclesEntity);
                return true;
            }
            return false;
        } catch (Exception e){
            log.error("An error occurred while deleting official vehicle " + e);
            return false;
        }
    }

    public List<OfficialVehiclesEntity> getAllOfficialVehicles(){
        List<OfficialVehiclesEntity> officialVehiclesList = officialVehiclesRepository.findAll();
        if (officialVehiclesList.isEmpty()){
            log.info("No se encontraron vehiculos oficiales");
        }
        return officialVehiclesList;
    }



}
