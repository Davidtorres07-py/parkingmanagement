package com.test.parking.staff.service;

import com.test.parking.staff.model.entity.OfficialRecordEntity;
import com.test.parking.staff.model.entity.ResidentPaymentEntity;
import com.test.parking.staff.model.entity.StayRegistrationEntity;
import com.test.parking.staff.model.entity.VehicleEntity;
import com.test.parking.staff.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@Service

public class StayRegistrationService {

    private final VehicleRepository vehicleRepository;
    private final OfficialVehiclesRepository officialVehiclesRepository;
    private final StayRegistrationRepository stayRegistrationRepository;
    private final ResidentPaymentRepository residentPaymentRepository;

    private final OfficialRecordRepository officialRecordRepository;

    public StayRegistrationService(VehicleRepository vehicleRepository, OfficialVehiclesRepository officialVehiclesRepository, StayRegistrationRepository stayRegistrationRepository, ResidentPaymentRepository residentPaymentRepository, OfficialRecordRepository officialRecordRepository) {
        this.vehicleRepository = vehicleRepository;
        this.officialVehiclesRepository = officialVehiclesRepository;
        this.stayRegistrationRepository = stayRegistrationRepository;
        this.residentPaymentRepository = residentPaymentRepository;
        this.officialRecordRepository = officialRecordRepository;
    }

    public void registerEntry(String placa){
        StayRegistrationEntity stayRegistrationEntity = new StayRegistrationEntity();
        stayRegistrationEntity.setPlaca(placa);
        LocalDateTime horaEntrada = LocalDateTime.now();
        stayRegistrationEntity.setEntrada(horaEntrada);

        stayRegistrationRepository.save(stayRegistrationEntity);
        log.info("Entrada registrada correctamente. Placa: {} Hora entrada: {}", placa,horaEntrada);
    }

    public void registerOut(String placa){
        StayRegistrationEntity stayRegistrationEntity = stayRegistrationRepository.findFirstByPlacaAndSalidaIsNullOrderByEntradaDesc(placa);
        LocalDateTime horaSalida = LocalDateTime.now();
        stayRegistrationEntity.setSalida(horaSalida);

        Integer tipoVehiculo = vehicleRepository.findTipoVehiculoIdByPlaca(placa);

        if (tipoVehiculo == null){
            tipoVehiculo = 3;
        }
        if (tipoVehiculo == 1){
            Optional<ResidentPaymentEntity> residentPaymentOptional = residentPaymentRepository.findByMatricula(placa);

            if (residentPaymentOptional.isPresent()){
                ResidentPaymentEntity residentPaymentEntity = residentPaymentOptional.get();
                long totalParkedMinutes = java.time.Duration.between(stayRegistrationEntity.getEntrada(), horaSalida).toMinutes();
                residentPaymentEntity.setTiempo_estacionado(residentPaymentEntity.getTiempo_estacionado() + totalParkedMinutes);
                residentPaymentEntity.setTotal(residentPaymentEntity.getTiempo_estacionado() * 0.05);

                residentPaymentRepository.save(residentPaymentEntity);
            } else {
                ResidentPaymentEntity newResidentPaymentEntity = new ResidentPaymentEntity();
                newResidentPaymentEntity.setMatricula(placa);
                newResidentPaymentEntity.setTiempo_estacionado(java.time.Duration.between(stayRegistrationEntity.getEntrada(),horaSalida).toMinutes());
                newResidentPaymentEntity.setTotal(newResidentPaymentEntity.getTiempo_estacionado()*0.05);

                residentPaymentRepository.save(newResidentPaymentEntity);
            }
        }
        if (tipoVehiculo == 2){
            Long idVehiculo = vehicleRepository.findIdByPlaca(placa);

            OfficialRecordEntity officialRecordEntity = new OfficialRecordEntity();
            officialRecordEntity.setId_vehiculo(idVehiculo);
            officialRecordEntity.setMatricula(placa);
            officialRecordEntity.setEntrada(stayRegistrationEntity.getEntrada());
            officialRecordEntity.setSalida(horaSalida);

            officialRecordRepository.save(officialRecordEntity);
        }

        stayRegistrationEntity.setCantidad_pago(totalAmountCalculator(stayRegistrationEntity.getEntrada(), horaSalida, tipoVehiculo));

        stayRegistrationRepository.save(stayRegistrationEntity);
        log.info("Salida registrada exitosamente");
    }

    private Double totalAmountCalculator(LocalDateTime entrada, LocalDateTime salida, Integer feeType){

        long minutesParked = java.time.Duration.between(entrada,salida).toMinutes();
        double finalFee = 0.5;

        if (feeType == 1 || feeType == 2){
            finalFee = 0;
        }

        return minutesParked * finalFee;
    }





}
