package com.test.parking.staff.controller;

import com.test.parking.staff.model.entity.StayRegistrationEntity;
import com.test.parking.staff.service.ResidentPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.test.parking.common.StaffConstants.BASE_PATH;

@Slf4j
@RestController
@RequestMapping(value = BASE_PATH)
public class ResidentPaymentController {

    private final ResidentPaymentService residentPaymentService;


    public ResidentPaymentController(ResidentPaymentService residentPaymentService) {
        this.residentPaymentService = residentPaymentService;
    }

    @GetMapping("/generateReport")
    public ResponseEntity<String> generateReport(@RequestParam String fileName) {
        List<StayRegistrationEntity> registrations = residentPaymentService.getAllRegistrations();
        residentPaymentService.generateResidentPaymentReport(fileName, registrations);
        return ResponseEntity.ok("Informe generado exitosamente.");
    }
}
