package com.test.parking.staff.controller;

import com.test.parking.staff.model.request.OfficialVehicleRequest;
import com.test.parking.staff.model.entity.OfficialVehiclesEntity;
import com.test.parking.staff.service.OfficialVehicleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.test.parking.common.StaffConstants.BASE_PATH;

@Slf4j
@RestController
@RequestMapping(value = BASE_PATH)
public class OfficialVehicleController {

    private final OfficialVehicleService officialVehicleService;


    public OfficialVehicleController(OfficialVehicleService officialVehicleService) {
        this.officialVehicleService = officialVehicleService;
    }

    @GetMapping("/officialVehicles/list")
    public ResponseEntity<?> getOfficialVehicles() {
        try {
            List<OfficialVehiclesEntity> officialVehiclesList = officialVehicleService.getAllOfficialVehicles();
            if (officialVehiclesList.isEmpty()) {
                log.info("No hay ningun vehiculo oficial registrado");
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("An error occurred while returning official vehicles");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/officialVehicleRegistration")
    public ResponseEntity<?> createOfficialVehicle(@RequestBody OfficialVehicleRequest request) {
        if (officialVehicleService.isRequestValid(request)) {
            ResponseEntity<OfficialVehiclesEntity> responseEntity = officialVehicleService.createOfficialVehicle(request);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                log.info("New official vehicle registered successfully");
                return new ResponseEntity<>(responseEntity, HttpStatus.OK);
            } else {
                // Manejar el caso en que la creación no fue exitosa
                log.error("Error occurred while creating official vehicle");
                return new ResponseEntity<>("An error occurred while creating official vehicle", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            // Manejar el caso en que la solicitud no es válida
            log.info("New official vehicle not registered due to invalid request");
            return new ResponseEntity<>("Invalid request for creating official vehicle", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/officialVehicle/{id}")
    public ResponseEntity<?> deleteOfficialVehicle (@PathVariable Long id) {
        try{
            if (officialVehicleService.deleteOfficialVehicle(id)){
                log.info("Official vehicle deleted succesfully. {}",id);
                return new ResponseEntity<>("Official vehicle deleted", HttpStatus.OK);
            } else {
                log.info("Official vehicle was not deleted {}", id);
                return new ResponseEntity<>("Official vehicle not deleted, try again", HttpStatus.INTERNAL_SERVER_ERROR);}
        } catch (Exception e){
            log.error("An error occured while deleting official vehicle with id {}", id + " Exception: "+ e.getMessage());
            return new ResponseEntity<>("Internal server error, please try again later.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
