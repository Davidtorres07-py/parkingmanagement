package com.test.parking.staff.controller;

import com.test.parking.staff.service.RecordManagementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.http.ResponseEntity;

import static com.test.parking.common.StaffConstants.BASE_PATH;

@RestController
@RequestMapping(value = BASE_PATH)
@Slf4j
public class RecordManagement {

    private final RecordManagementService recordManagementService;

    public RecordManagement(RecordManagementService recordManagementService) {
        this.recordManagementService = recordManagementService;
    }

    @PostMapping("/beginMonth")
    public ResponseEntity<String> beginMonth() {
        try {
            recordManagementService.startNewMonth();
            log.info("Mes comenzado correctamente. Estancias en coches oficiales eliminadas y tiempo estacionado por vehículos de residentes puesto a cero.");
            return ResponseEntity.ok("Mes comenzado correctamente.");
        } catch (Exception e) {
            log.error("Error al comenzar el mes: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error al comenzar el mes.");
        }
    }

}
