package com.test.parking.staff.controller;


import com.test.parking.staff.service.StayRegistrationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.test.parking.common.StaffConstants.BASE_PATH;

@Slf4j
@RestController
@RequestMapping(BASE_PATH)
public class StayRegistrationController {

    private final StayRegistrationService stayRegistrationService;

    public StayRegistrationController(StayRegistrationService stayRegistrationService) {
        this.stayRegistrationService = stayRegistrationService;
    }

    @PostMapping("/registerEntry")
    public ResponseEntity<String> registerEntry(@RequestParam String placa){
        stayRegistrationService.registerEntry(placa);
        return new ResponseEntity<>("Entrada registrada exitosamente", HttpStatus.OK);
    }

    @PostMapping("/registerOut")
    public ResponseEntity<String> registerOut(@RequestParam String placa){
        stayRegistrationService.registerOut(placa);
        return new ResponseEntity<>("Salida registrada exitosamente", HttpStatus.OK);
    }

}
