package com.test.parking.staff.controller;

import com.test.parking.staff.model.entity.OfficialVehiclesEntity;
import com.test.parking.staff.model.entity.ResidentVehiclesEntity;
import com.test.parking.staff.service.ResidentVehicleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.test.parking.common.StaffConstants.BASE_PATH;

@Slf4j
@RestController
@RequestMapping(value = BASE_PATH)
public class ResidentVehicleController {

    private final ResidentVehicleService residentVehicleService;

    public ResidentVehicleController(ResidentVehicleService residentVehicleService) {
        this.residentVehicleService = residentVehicleService;
    }

    @GetMapping("/residentVehicles/list")
    public ResponseEntity<?> getOfficialVehicles() {
        try {
            List<String> officialVehiclesList = residentVehicleService.getAllResidentVehicles();
            if (officialVehiclesList.isEmpty()) {
                log.info("No hay ningun vehiculo residente registrado");
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("An error occurred while returning resident vehicles");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/residentVehicleRegistration")
    public ResponseEntity<?> createResidentVehicle(@RequestParam String placa) {
        ResponseEntity<ResidentVehiclesEntity> responseEntity = residentVehicleService.createResidentVehicle(placa);

        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            log.info("New resident vehicle registered successfully");
            return new ResponseEntity<>(responseEntity, HttpStatus.OK);
        } else {
            // Handle the case where the creation was not successful
            log.error("Error occurred while creating resident vehicle");
            return new ResponseEntity<>("An error occurred while creating resident vehicle", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/residentVehicle/{id}")
    public ResponseEntity<String> deleteResidentVehicle(@PathVariable Long id) {
        Boolean deleted = residentVehicleService.deleteResidentVehicle(id);

        if (deleted) {
            log.info("Resident vehicle with ID {} deleted successfully", id);
            return ResponseEntity.ok("Resident vehicle deleted successfully");
        } else {
            log.error("Error occurred while deleting resident vehicle with ID {}", id);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Resident vehicle not found or could not be deleted");
        }
    }



}
