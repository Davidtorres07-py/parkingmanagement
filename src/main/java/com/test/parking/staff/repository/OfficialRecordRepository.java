package com.test.parking.staff.repository;

import com.test.parking.staff.model.entity.OfficialRecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfficialRecordRepository extends JpaRepository<OfficialRecordEntity, Long> {

}
