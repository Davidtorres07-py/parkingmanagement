package com.test.parking.staff.repository;

import com.test.parking.staff.model.entity.VehicleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VehicleRepository extends JpaRepository<VehicleEntity, Long> {
    @Query("SELECT COALESCE(v.id_tipo, 3) FROM VehicleEntity v WHERE v.placa = :placa")
    Integer findTipoVehiculoIdByPlaca(@Param("placa") String placa);

    @Query("SELECT v.id FROM VehicleEntity v WHERE v.placa = :placa")
    Long findIdByPlaca(@Param("placa") String placa);


}
