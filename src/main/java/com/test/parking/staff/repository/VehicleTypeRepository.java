package com.test.parking.staff.repository;

import com.test.parking.staff.model.entity.VehicleTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleTypeRepository extends JpaRepository<VehicleTypeEntity,Long> {

}
