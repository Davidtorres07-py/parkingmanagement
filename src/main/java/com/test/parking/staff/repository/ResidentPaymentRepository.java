package com.test.parking.staff.repository;

import com.test.parking.staff.model.entity.ResidentPaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ResidentPaymentRepository extends JpaRepository<ResidentPaymentEntity, Long> {
    Optional<ResidentPaymentEntity> findByMatricula(String placa);

}
