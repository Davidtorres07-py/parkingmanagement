package com.test.parking.staff.repository;

import com.test.parking.staff.model.entity.OfficialVehiclesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfficialVehiclesRepository extends JpaRepository<OfficialVehiclesEntity, Long> {

}
