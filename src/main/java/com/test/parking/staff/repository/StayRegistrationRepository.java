package com.test.parking.staff.repository;

import com.test.parking.staff.model.entity.StayRegistrationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import com.test.parking.staff.model.entity.StayRegistrationEntity;

public interface StayRegistrationRepository extends JpaRepository<StayRegistrationEntity, Long> {

    StayRegistrationEntity findFirstByPlacaAndSalidaIsNullOrderByEntradaDesc(String placa);
}
