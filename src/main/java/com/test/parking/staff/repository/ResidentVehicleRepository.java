package com.test.parking.staff.repository;

import com.test.parking.staff.model.entity.ResidentVehiclesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResidentVehicleRepository extends JpaRepository<ResidentVehiclesEntity, Long>{

}
