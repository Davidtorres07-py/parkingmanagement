package com.test.parking.staff.model.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "pagoresidentes")

public class ResidentPaymentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int id_vehiculo;
    private String matricula;
    private Long tiempo_estacionado;
    private Double total;
}
