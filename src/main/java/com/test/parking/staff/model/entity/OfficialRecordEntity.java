package com.test.parking.staff.model.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@Table(name = "registrooficiales")
public class OfficialRecordEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
    private Long id_vehiculo;
    private String matricula;
    private LocalDateTime entrada;
    private LocalDateTime salida;

}
