package com.test.parking.staff.model.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class OfficialVehicleRequest {

    private String placa;

}
